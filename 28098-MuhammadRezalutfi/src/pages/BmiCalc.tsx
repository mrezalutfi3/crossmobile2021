import { Redirect, Route } from 'react-router-dom';
import { IonApp, IonButton, IonCard, IonCol, IonContent, IonFooter, IonCardContent, IonGrid, IonHeader, IonIcon, IonInput, IonItem, IonLabel, IonRouterOutlet, IonRow, IonTitle, IonToolbar, IonAlert } from '@ionic/react';
import { IonReactRouter } from '@ionic/react-router';
import { calculatorOutline, refreshOutline } from 'ionicons/icons';
import { useRef, useState } from "react";
import Home from '../pages/Home';
import BmiControls from '../components/BmiControls';

/* Core CSS required for Ionic components to work properly */
import '@ionic/react/css/core.css';

/* Basic CSS for apps built with Ionic */
import '@ionic/react/css/normalize.css';
import '@ionic/react/css/structure.css';
import '@ionic/react/css/typography.css';

/* Optional CSS utils that can be commented out */
import '@ionic/react/css/padding.css';
import '@ionic/react/css/float-elements.css';
import '@ionic/react/css/text-alignment.css';
import '@ionic/react/css/text-transformation.css';
import '@ionic/react/css/flex-utils.css';
import '@ionic/react/css/display.css';

/* Theme variables */
import '../theme/variables.css';
import InputControl from '../components/InputControls';

const BmiCalc: React.FC = () => {
  const [ calculatedBMI, setCalculatedBMI ] = useState<number>(0);
  const [ category, setCategory ] = useState<string>("");
  const heightInputRef = useRef<HTMLIonInputElement>(null);
  const weightInputRef = useRef<HTMLIonInputElement>(null);
  const [ error, setError ] = useState<string>();
  const [ calcUnits, setCalcUnit] = useState<'cmkg' | 'ftlbs'>('cmkg');

  const calculateBMI = () => {
    let enteredWeight, enteredHeight;
    if (!heightInputRef.current || !weightInputRef.current || !weightInputRef.current.value || !heightInputRef.current.value) {
      return;
    }
    if (calcUnits === "cmkg") {
        enteredWeight = Number(weightInputRef.current.value);
        enteredHeight = Number(heightInputRef.current.value) / 100;
      } else if (calcUnits === "ftlbs") {
        enteredWeight = Number(weightInputRef.current.value) * 0.453;
        enteredHeight = Number(heightInputRef.current.value) * 0.3048;
      } else {
        return;
      }

    if(!enteredWeight || !enteredHeight) return;
    const bmi = +enteredWeight / ((+enteredHeight) * (+enteredHeight));

    if (bmi < 18.5) {
         setCategory("Kurus");
    } else if (bmi <= 24.9) {
         setCategory("Normal");
    } else if (bmi <= 29.9) {
         setCategory("Gemuk");
    } else {
         setCategory("Obesitas");
    }

    console.log(bmi);
    setCalculatedBMI(bmi);

    if(!enteredWeight || !enteredHeight || +enteredHeight <= 0 || +enteredWeight <= 0) {
        setError('Please enter a valid (non-negative) input number');
        return;
    }

  };

  const resetInputs = () => {
    weightInputRef.current!.value = '';
    heightInputRef.current!.value = '';

  };

  const clearError = () => {
    setError("");
  };

  const selectCalcUnitHandler = (selectedValue: 'cmkg' | 'ftlbs') => {
      setCalcUnit(selectedValue);
  }

  return (

    <>
    <IonAlert
    isOpen={!!error}
    message={error}
    buttons={[
        {text: 'Okay', handler: clearError}
    ]}/> 

  <IonApp>
    <IonHeader>
        <IonToolbar color="primary">
            <IonTitle>Kalkulator BMI</IonTitle>
        </IonToolbar>
    </IonHeader>
    <IonContent className="ion-padding">
        <InputControl selectedValue={calcUnits} onSelectValue={selectCalcUnitHandler}/>
        <IonItem>
            <IonLabel position="floating">Tinggi Badan({calcUnits === 'cmkg' ? 'cm' : 'feet'})</IonLabel>
            <IonInput type="number" ref={heightInputRef}></IonInput>
        </IonItem>
        <IonItem>
            <IonLabel position="floating">Berat Badan({calcUnits === 'cmkg' ? 'kg' : 'lbs'})</IonLabel>
            <IonInput type="number" ref={weightInputRef}></IonInput>
        </IonItem>
        <IonContent className="ion-padding">
          <IonGrid class="ion-text-center ion-margin">
              <IonRow>
                  <IonCol size-sm="8" offset-sm="2" size-md="6" offset-md="3">
                      <IonButton onClick={calculateBMI}>
                          <IonIcon slot="start" icon={calculatorOutline}></IonIcon>
                          Hitung BMI
                      </IonButton>
                  </IonCol>
                  <IonCol size-sm="8" offset-sm="2" size-md="6" offset-md="3">
                      <IonButton onClick={resetInputs} fill="outline">
                          <IonIcon slot="start" icon={refreshOutline}></IonIcon>
                          Reset
                      </IonButton>
                  </IonCol>
              </IonRow>
              <BmiControls onCalculate={calculateBMI} onReset={resetInputs}/>
              {calculatedBMI && (
              <IonRow>
                  <IonCol>
                      <IonCard>
                          <IonCardContent className="ion-text-center">
                            <h2>{calculatedBMI}</h2>
                            {<h3>{category}</h3>}
                          </IonCardContent>
                      </IonCard>
                  </IonCol>
              </IonRow>
              )}
              
          </IonGrid>
        </IonContent>
    </IonContent>
    <IonFooter>
        <IonToolbar color="primary">
            <IonTitle class="ion-text-center ion-margin">00000028098_MuhammadRezalutfi_Week01</IonTitle>
        </IonToolbar>
    </IonFooter>
  </IonApp>
  </>
  )
};



export default BmiCalc;
