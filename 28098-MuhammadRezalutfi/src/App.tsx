import { Redirect, Route } from 'react-router-dom';
import { IonApp, IonButton, IonCard, IonCol, IonContent, IonFooter, IonCardContent, IonGrid, IonHeader, IonIcon, IonInput, IonItem, IonLabel, IonRouterOutlet, IonRow, IonTitle, IonToolbar, IonAlert } from '@ionic/react';
import { IonReactRouter } from '@ionic/react-router';
import { calculatorOutline, refreshOutline } from 'ionicons/icons';
import { useRef, useState } from "react";
import Home from './pages/Home';
import BmiControls from './components/BmiControls';

/* Core CSS required for Ionic components to work properly */
import '@ionic/react/css/core.css';

/* Basic CSS for apps built with Ionic */
import '@ionic/react/css/normalize.css';
import '@ionic/react/css/structure.css';
import '@ionic/react/css/typography.css';

/* Optional CSS utils that can be commented out */
import '@ionic/react/css/padding.css';
import '@ionic/react/css/float-elements.css';
import '@ionic/react/css/text-alignment.css';
import '@ionic/react/css/text-transformation.css';
import '@ionic/react/css/flex-utils.css';
import '@ionic/react/css/display.css';

/* Theme variables */
import './theme/variables.css';
import InputControl from './components/InputControls';
import BmiCalc from './pages/BmiCalc';

const App: React.FC = () => (
  <IonApp>
      <IonReactRouter>
          <IonRouterOutlet>
              <Route exact path="/home" component={Home}/>
              <Redirect exact from="/" to="/home"/>
              <Route path="/bmi" component={BmiCalc}/>
          </IonRouterOutlet>
      </IonReactRouter>
  </IonApp>
);



export default App;
