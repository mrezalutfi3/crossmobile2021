import React from 'react';

import { Redirect, Route } from 'react-router-dom';
import { IonApp, IonButton, IonCard, IonCol, IonContent, IonFooter, IonCardContent, IonGrid, IonHeader, IonIcon, IonInput, IonItem, IonLabel, IonRouterOutlet, IonRow, IonTitle, IonToolbar, IonSegment, IonSegmentButton } from "@ionic/react";
import { IonReactRouter } from '@ionic/react-router';
import { calculatorOutline, refreshOutline } from "ionicons/icons";
import { useRef, useState } from "react";

const InputControl: React.FC<{
    selectedValue: 'cmkg' | 'ftlbs';
    onSelectValue: (value: 'cmkg' | 'ftlbs') => void
}> = props => {
    
    const InputChangeHandler = (event:CustomEvent) => {
        props.onSelectValue(event.detail.value);
    };
    
    return (
        <IonSegment value={props.selectedValue} onIonChange={InputChangeHandler}>
            <IonSegmentButton value="cmkg">
                <IonLabel>cm/kg</IonLabel>
            </IonSegmentButton>
            <IonSegmentButton value="ftlbs">
                <IonLabel>ft/lbs</IonLabel>
            </IonSegmentButton>
        </IonSegment>
    );
};

export default InputControl;