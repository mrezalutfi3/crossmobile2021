import React from 'react';

import { Redirect, Route } from 'react-router-dom';
import { IonApp, IonButton, IonCard, IonCol, IonContent, IonFooter, IonCardContent, IonGrid, IonHeader, IonIcon, IonInput, IonItem, IonLabel, IonRouterOutlet, IonRow, IonTitle, IonToolbar } from "@ionic/react";
import { IonReactRouter } from '@ionic/react-router';
import { calculatorOutline, refreshOutline } from "ionicons/icons";
import { useRef, useState } from "react";

const BmiControls: React.FC<{onCalculate: () => void; onReset: () => void}> = props => {
    return (
    <IonRow>
        <IonCol size="12" size-md="6" className="ion-text-center">
            <IonButton expand="block" color="success" onClick={props.onCalculate}>
                <IonIcon slot="start" icon={calculatorOutline}></IonIcon>
                Hitung BMI
            </IonButton>
        </IonCol>
        <IonCol size="12" size-md="6" className="ion-text-center">
            <IonButton fill="clear" color="medium" onClick={props.onReset}>
                <IonIcon slot="start" icon={refreshOutline}></IonIcon>
                Reset
            </IonButton>
        </IonCol>
    </IonRow>
    );
};

export default BmiControls;